import React, { useState } from "react";
import PropTypes from "prop-types";

function FormAddress({ goNext, setDataInfo, info }) {
  const [textAddress, setTextAddress] = useState("");
  const [textError, setTextError] = useState(null);

  useState(() => {
    setTextAddress(info.address);
  }, []);

  const errorAnimation = () => {
    const element = document.querySelector(".form-control");
    element.classList.add(
      "animate__animated",
      "animate__headShake",
      "is-invalid"
    );
    element.addEventListener("animationend", () => {
      element.classList.remove("animate__animated", "animate__headShake");
    });
  };

  const onClickNext = () => {
    if (successValid(textAddress)) {
      goNext();
      setDataInfo({ ...info, address: textAddress });
    } else {
      errorAnimation();
    }
  };

  const setValueAddress = (e) => {
    const { value } = e.target;
    setTextAddress(value);
  };

  const successValid = (value) => {
    if (value.length == 0) {
      setTextError("El campo es obligatorio.");
      return false;
    }
    if (value.length < 6) {
      setTextError("El campo debe ser mayor o igual a 7.");
      return false;
    }
    if (value.length > 150) {
      setTextError("El campo debe ser menor o igual a 150.");
      return false;
    }
    return true;
  };

  return (
    <div class="animate__animated animate__bounceInRight">
      <div class="form-group">
        <label> Dirección del apartamento</label>
        <input
          type="text"
          value={textAddress}
          onChange={setValueAddress}
          class="form-control form-control-user"
          placeholder="Cra 1W # 42 - 100"
        />
        {textError && <div class="invalid-feedback">{textError}</div>}
      </div>
      <button
        class="btn btn-primary btn-user btn-block"
        onClick={() => onClickNext()}
      >
        Continuar
      </button>
    </div>
  );
}

FormAddress.defaultProps = {
  goNext: () => {},
  info: {
    address: "",
  },
};

FormAddress.propTypes = {
  goNext: PropTypes.func.isRequired,
  setDataInfo: PropTypes.func.isRequired,
  info: PropTypes.shape({
    address: PropTypes.string,
  }),
  listStep: PropTypes.array,
};

export default FormAddress;
