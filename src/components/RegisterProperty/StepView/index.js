import PropTypes from "prop-types";

import "./index.css";

function StepView({ step, toNavigation, listStep, responsive }) {
  const responsiveRes = responsive
    ? ""
    : "d-none animate__animated animate__bounceInDown";

  const heightLine = step > 5 ? 4 * 60 : step == 0 ? 0 : (step - 1) * 60;
  return (
    <div
      class={`col-lg-5 ${responsiveRes} d-lg-block`}
      style={{ background: "#f8f9fc" }}
    >
      <div style={{ marginTop: responsive ? 0 : 50, height: 360 }}>
        <div class="box-step">
          {listStep.map((item) => {
            return (
              <div
                class="item-step"
                onClick={() =>
                  step >= item.id
                    ? toNavigation({ route: item.route, step: item.id })
                    : null
                }
              >
                <div
                  class="btn btn-light btn-circle shadow"
                  style={{
                    backgroundColor: step > item.id ? "#8512ff" : "white",
                  }}
                >
                  <i
                    class="fa fa-check"
                    style={{ color: step > item.id ? "white" : "#8512ff" }}
                  ></i>
                </div>
                <div style={{ width: 20 }} />
                <div>{item.label}</div>
              </div>
            );
          })}
        </div>
        <div class="line-step" style={{ height: heightLine }} />
      </div>
    </div>
  );
}

StepView.defaultProps = {
  toNavigation: () => {},
  step: 0,
  listStep: [],
  responsive: false,
};

StepView.propTypes = {
  toNavigation: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired,
  listStep: PropTypes.array,
  responsive: PropTypes.bool,
};

export default StepView;
