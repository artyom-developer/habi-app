import React, { useState } from "react";
import PropTypes from "prop-types";

function FormName({ goNext, setDataInfo, info }) {
  const [textName, setTextName] = useState("");
  const [textError, setTextError] = useState(null);

  useState(() => {
    setTextName(info.fullname);
  }, []);

  const errorAnimation = () => {
    const element = document.querySelector(".form-control");
    element.classList.add(
      "animate__animated",
      "animate__headShake",
      "is-invalid"
    );
    element.addEventListener("animationend", () => {
      element.classList.remove("animate__animated", "animate__headShake");
    });
  };

  const onClickNext = () => {
    if (successValid(textName) && textName.length > 5) {
      setDataInfo({ ...info, fullname: textName });
      goNext();
    }
  };

  const setValueName = (e) => {
    const element = document.querySelector(".form-control");
    const { value } = e.target;
    if (!successValid(value)) {
      errorAnimation();
    } else if (successValid(value) && value.length >= 3) {
      element.classList.remove("is-invalid");
      element.classList.add("is-valid");
      setTextError(null);
    } else {
      element.classList.remove("is-invalid", "is-valid");
      setTextError(null);
    }
    setTextName(value);
  };

  const successValid = (value) => {
    const lettervalid = /^[A-Z]+$/i;
    if (value.length == 0) {
      setTextError("El campo es obligatorio.");
      return false;
    }
    if (!lettervalid.test(value.replace(/\s/g, ""))) {
      setTextError("El campo solo puede contener letras.");
      return false;
    }
    if (value.length > 80) {
      setTextError("El campo debe ser menor o igual a 80.");
      return false;
    }
    return true;
  };

  return (
    <div class="animate__animated animate__bounceInRight">
      <div class="form-group">
        <label>Nombre y Apellido</label>
        <input
          type="text"
          value={textName}
          onChange={setValueName}
          class="form-control form-control-user"
          placeholder="John Doe"
        />
        {textError && <div class="invalid-feedback">{textError}</div>}
      </div>
      <button
        class="btn btn-primary btn-user btn-block"
        onClick={() => onClickNext()}
      >
        Continuar
      </button>
    </div>
  );
}

FormName.defaultProps = {
  goNext: () => {},
  info: {
    fullname: "",
  },
};

FormName.propTypes = {
  goNext: PropTypes.func.isRequired,
  setDataInfo: PropTypes.func.isRequired,
  info: PropTypes.shape({
    fullname: PropTypes.string,
  }),
};

export default FormName;
