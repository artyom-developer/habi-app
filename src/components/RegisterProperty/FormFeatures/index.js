import React, { useState } from "react";
import PropTypes from "prop-types";

import "./index.css";

function FormFeatures({ goNext, setDataInfo, info, iconFeatures }) {
  const [features, setFeatures] = useState([]);

  useState(() => {
    setFeatures(info.features);
  }, []);

  const onClickNext = () => {
    setDataInfo({ ...info, features });
    goNext();
  };

  const setValueFeature = (item) => {
    var data = features;
    const index = data.findIndex((element) => element.id === item.id);
    if (index >= 0) {
      data.splice(index, 1);
      setFeatures((arr) => [...data]);
    } else {
      setFeatures((arr) => [...arr, item]);
    }
  };

  const hasFeature = (e) => {
    try {
      const res = features.filter((item) => item.id === e.id);
      if (res.length >= 1) return true;
      return false;
    } catch (error) {
      console.log(error.message);
      return false;
    }
  };

  return (
    <div class="animate__animated animate__bounceInRight">
      <label>Características</label>
      <div class="form-group box-center">
        {iconFeatures.map((item) => {
          const has = hasFeature(item);
          return (
            <div class="item-step" onClick={() => setValueFeature(item)}>
              <div
                class="btn btn-light btn-circle btn-lg shadow"
                style={
                  has
                    ? { borderWidth: 2, borderColor: "#8512ff" }
                    : { backgroundColor: "white" }
                }
              >
                {item.icon}
              </div>
              <div style={{ width: 10 }} />
              <div style={{ color: has ? "#8512ff" : null }}>{item.label} </div>
            </div>
          );
        })}
      </div>
      <button
        class="btn btn-primary btn-user btn-block"
        onClick={() => onClickNext()}
      >
        Finalizar
      </button>
    </div>
  );
}

FormFeatures.defaultProps = {
  goNext: () => {},
  info: {
    features: "",
  },
  iconFeatures: [],
};

FormFeatures.propTypes = {
  goNext: PropTypes.func.isRequired,
  setDataInfo: PropTypes.func.isRequired,
  info: PropTypes.shape({
    features: PropTypes.array,
  }),
  iconFeatures: PropTypes.array.isRequired,
};

export default FormFeatures;
