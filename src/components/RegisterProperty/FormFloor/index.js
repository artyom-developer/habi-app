import React, { useState } from "react";
import PropTypes from "prop-types";

function FormFloor({ goNext, setDataInfo, info }) {
  const [textFloor, setTextFloor] = useState("");
  const [textError, setTextError] = useState(null);

  useState(() => {
    setTextFloor(info.floor);
  }, []);

  const errorAnimation = () => {
    const element = document.querySelector(".form-control");
    element.classList.add(
      "animate__animated",
      "animate__headShake",
      "is-invalid"
    );
    element.addEventListener("animationend", () => {
      element.classList.remove("animate__animated", "animate__headShake");
    });
  };

  const onClickNext = () => {
    if (successValid(textFloor)) {
      setDataInfo({ ...info, floor: textFloor });
      goNext();
    }
  };

  const setValueFloor = (e) => {
    const element = document.querySelector(".form-control");
    const { value } = e.target;
    if (!successValid(value)) {
      errorAnimation();
    } else if (successValid(value)) {
      element.classList.remove("is-invalid");
      element.classList.add("is-valid");
      setTextError(null);
    } else {
      element.classList.remove("is-invalid", "is-valid");
      setTextError(null);
    }
    setTextFloor(value);
  };

  const successValid = (value) => {
    var validNumber = /^[0-9]+$/;
    if (!value.match(validNumber)) {
      setTextError("El campo solo debe contener numéricos.");
      return false;
    }
    if (value.length == 0 || value === "") {
      setTextError("El campo es obligatorio.");
      return false;
    }
    if (value == 0) {
      setTextError("El campo debe ser mayor o igual a 1.");
      return false;
    }
    if (value > 50) {
      setTextError("El campo debe ser menor a 50.");
      return false;
    }
    return true;
  };

  return (
    <div class="animate__animated animate__bounceInRight">
      <div class="form-group">
        <label>Número de piso </label>
        <input
          type="text"
          value={textFloor}
          onChange={setValueFloor}
          class="form-control form-control-user"
          placeholder=" "
        />
        {textError && <div class="invalid-feedback">{textError}</div>}
      </div>
      <button
        class="btn btn-primary btn-user btn-block"
        onClick={() => onClickNext()}
      >
        Continuar
      </button>
    </div>
  );
}

FormFloor.defaultProps = {
  goNext: () => {},
  info: {
    floor: "",
  },
};

FormFloor.propTypes = {
  goNext: PropTypes.func.isRequired,
  setDataInfo: PropTypes.func.isRequired,
  info: PropTypes.shape({
    floor: PropTypes.number,
  }),
};

export default FormFloor;
