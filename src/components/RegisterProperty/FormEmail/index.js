import React, { useState } from "react";
import PropTypes from "prop-types";

function FormAddress({ goNext, setDataInfo, info }) {
  const [textEmail, setTextEmail] = useState("");
  const [textError, setTextError] = useState(null);

  useState(() => {
    setTextEmail(info.email);
  }, []);

  const errorAnimation = () => {
    const element = document.querySelector(".form-control");
    element.classList.add(
      "animate__animated",
      "animate__headShake",
      "is-invalid"
    );
    element.addEventListener("animationend", () => {
      element.classList.remove("animate__animated", "animate__headShake");
    });
  };

  const onClickNext = () => {
    if (successValid(textEmail)) {
      setDataInfo({ ...info, email: textEmail });
      goNext();
    } else {
      errorAnimation();
    }
  };

  const setValueEmail = (e) => {
    const { value } = e.target;
    setTextEmail(value);
    if (successValid(value)) {
      const element = document.querySelector(".form-control");
      element.classList.add("is-valid");
    }
  };

  const successValid = (value) => {
    let validEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (value.length == 0) {
      setTextError("El campo es obligatorio.");
      return false;
    }
    if (!validEmail.test(value)) {
      setTextError("El campo debe ser una dirección de correo válida.");
      return false;
    }
    if (value.length > 150) {
      setTextError("El campo debe ser menor o igual a 150.");
      return false;
    }
    return true;
  };

  return (
    <div class="animate__animated animate__bounceInRight">
      <div class="form-group">
        <label>Correo electronico</label>
        <input
          type="text"
          value={textEmail}
          onChange={setValueEmail}
          class="form-control form-control-user"
          placeholder="ejemplo@habi.com"
        />
        {textError && <div class="invalid-feedback">{textError}</div>}
      </div>
      <button
        class="btn btn-primary btn-user btn-block"
        onClick={() => onClickNext()}
      >
        Continuar
      </button>
    </div>
  );
}

FormAddress.defaultProps = {
  goNext: () => {},
  info: {
    email: "",
  },
};

FormAddress.propTypes = {
  goNext: PropTypes.func.isRequired,
  setDataInfo: PropTypes.func.isRequired,
  info: PropTypes.shape({
    email: PropTypes.string,
  }),
};

export default FormAddress;
