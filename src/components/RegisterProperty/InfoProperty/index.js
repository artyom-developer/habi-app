import PropTypes from "prop-types";

function InfoProperty({ info, listStep, toNavigation, setStep }) {
  return (
    <div class="animate__animated animate__bounceInRight">
      {listStep.map((item) => {
        return (
          <div>
            <div>
              <i
                class="fas fa-edit"
                fill="#8512ff"
                onClick={() =>
                  toNavigation({ route: item.route, step: item.id })
                }
              ></i>
              <span class="my-0" style={{ color: "#8512ff" }}>
                {" "}
                <b>{item.label}</b>
              </span>
            </div>
            {item.id == 5 ? (
              info[item.field].map((ic) => {
                return (
                  <div class="item-step">
                    <div
                      class="btn btn-light btn-circle shadow"
                      style={{ backgroundColor: "white" }}
                    >
                      {ic.icon}
                    </div>
                    <div style={{ width: 10 }} />
                    <div>{ic.label}</div>
                  </div>
                );
              })
            ) : (
              <span class="text-muted">{info[item.field]}</span>
            )}
          </div>
        );
      })}
    </div>
  );
}

InfoProperty.defaultProps = {
  toNavigation: () => {},
  info: {
    fullname: "",
    email: "",
    address: "",
    floor: "",
    features: [],
  },
  listStep: [],
  iconFeatures: [],
};

InfoProperty.propTypes = {
  toNavigation: PropTypes.func.isRequired,
  info: PropTypes.shape({
    fullname: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.string,
    floor: PropTypes.number,
    features: PropTypes.array,
  }),
  listStep: PropTypes.array,
  iconFeatures: PropTypes.array,
};

export default InfoProperty;
