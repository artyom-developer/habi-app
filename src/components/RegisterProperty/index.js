import React, { useState } from "react";

import { ReactComponent as IconHome } from "../../icons/home.svg";
import { ReactComponent as IconPark } from "../../icons/park.svg";
import { ReactComponent as IconBBQ } from "../../icons/bbq.svg";

import FormName from "./FormName";
import FormEmail from "./FormEmail";
import FormAddress from "./FormAddress";
import FormFloor from "./FormFloor";
import FormFeatures from "./FormFeatures";
import StepView from "./StepView";
import InfoProperty from "./InfoProperty";

import "./index.css";

import { Routes, Route, useNavigate } from "react-router-dom";

const listStep = [
  {
    id: 1,
    label: "Nombre y Apellido",
    field: "fullname",
    route: "/datos-cliente",
  },
  {
    id: 2,
    label: "Correo electronico",
    field: "email",
    route: "/email-cliente",
  },
  {
    id: 3,
    label: "Dirección",
    field: "address",
    route: "/direccion-apartamento",
  },
  {
    id: 4,
    label: "Número de piso",
    field: "floor",
    route: "/piso-apartamento",
  },
  {
    id: 5,
    label: "Caracteristicas",
    field: "features",
    route: "/caracteristicas-apartamento",
  },
];

const iconFeatures = [
  {
    id: 1,
    label: "Salón comunal",
    icon: <IconHome fill="#8512ff" width="60" height="60" />,
  },
  {
    id: 2,
    label: "Zona BBQ",
    icon: <IconBBQ fill="#8512ff" width="60" height="60" />,
  },
  {
    id: 3,
    label: "Parque de juegos",
    icon: <IconPark fill="#8512ff" width="60" height="60" />,
  },
];

function Main({ goStart }) {
  let navigate = useNavigate();
  return (
    <div style={{ paddingVertical: 100 }}>
      <button class="btn btn-primary btn-block" onClick={goStart}>
        Iniciar
      </button>
    </div>
  );
}

function RegisterProperty() {
  let navigate = useNavigate();

  const [step, setStep] = useState(0);
  const [info, setInfo] = useState({
    fullname: "",
    email: "",
    address: "",
    floor: "",
    features: [],
  });

  const navigation = ({ route, step }) => {
    setStep(step);
    navigate(route);
  };
  return (
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <div class="row">
          <div class="col-lg-7">
            {step > 0 && step < 6 && (
              <div class="step-number">
                <p> {step} / 5</p>
              </div>
            )}
            <div class="p-5 box-form">
              <div class="text-center">
                <img src="habi-morado.png" alt="new" width={100} height={100} />
              </div>

              <Routes>
                <Route
                  path="/"
                  element={
                    <Main
                      goStart={() =>
                        navigation({ route: "datos-cliente", step: 1 })
                      }
                    />
                  }
                />
                <Route
                  path="/datos-cliente"
                  element={
                    <FormName
                      goNext={() =>
                        navigation({ route: "email-cliente", step: 2 })
                      }
                      setDataInfo={setInfo}
                      info={info}
                    />
                  }
                />
                <Route
                  path="/email-cliente"
                  element={
                    <FormEmail
                      goNext={() =>
                        navigation({ route: "direccion-apartamento", step: 3 })
                      }
                      setDataInfo={setInfo}
                      info={info}
                    />
                  }
                />
                <Route
                  path="/direccion-apartamento"
                  element={
                    <FormAddress
                      goNext={() =>
                        navigation({ route: "piso-apartamento", step: 4 })
                      }
                      setDataInfo={setInfo}
                      info={info}
                    />
                  }
                />
                <Route
                  path="/piso-apartamento"
                  element={
                    <FormFloor
                      goNext={() =>
                        navigation({
                          route: "caracteristicas-apartamento",
                          step: 5,
                        })
                      }
                      setDataInfo={setInfo}
                      info={info}
                    />
                  }
                />
                <Route
                  path="/caracteristicas-apartamento"
                  element={
                    <FormFeatures
                      goNext={() => navigation({ route: "resumen", step: 6 })}
                      iconFeatures={iconFeatures}
                      setDataInfo={setInfo}
                      info={info}
                    />
                  }
                />
                <Route
                  path="/resumen"
                  element={
                    <InfoProperty
                      toNavigation={(data) => navigation(data)}
                      listStep={listStep}
                      info={info}
                    />
                  }
                />
              </Routes>
              <div class="text-center">
                <hr />
                <a class="small" href="#">
                  Developed by Luis Felipe Perdomo Murcia
                </a>
              </div>
            </div>
          </div>
          {step > 5 || step == 0 ? (
            <div class="col-lg-5 d-none d-lg-block bg-register-image animate__animated animate__bounceInRight"></div>
          ) : (
            <StepView
              step={step}
              toNavigation={(data) => navigation(data)}
              listStep={listStep}
              responsive={false}
            />
          )}
        </div>
      </div>

      <div
        class="modal fade"
        id="resumeModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="resumeModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" role="document">
          <div class="card shadow ">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Resumen</h6>
            </div>
            <div class="card-body">
              <StepView
                step={step}
                setStep={setStep}
                listStep={listStep}
                responsive={true}
              />
            </div>
          </div>
        </div>
      </div>

      <a
        href="#"
        class="btn btn-info btn-icon-split btn-resume"
        data-toggle="modal"
        data-target="#resumeModal"
      >
        <span class="icon">
          <i class="fas fa-info-circle"></i>
        </span>
        <span class="text">Resumen</span>
      </a>
    </div>
  );
}

export default RegisterProperty;
