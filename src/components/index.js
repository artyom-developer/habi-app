import React from 'react';
import './index.css';

import RegisterProperty from "./RegisterProperty"

import { BrowserRouter } from "react-router-dom"; 
 
function Index() { 
  return (
    <BrowserRouter>
      <div class="container"> 
        <RegisterProperty/>    
      </div>
    </BrowserRouter>
  );
}

export default Index;
